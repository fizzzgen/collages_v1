import logging
import os

from io import BytesIO

from flask import Flask
from flask import send_file
from flask import request
from flask import render_template

from flask.cli import FlaskGroup

from collage import create_collage, NUMERIC_PARAMS
from search import ImageSearch


_image_searcher = None
app = Flask(__name__,  template_folder="static")

logger = logging.getLogger('flask_app')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('static/flask_app.log')
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)


def create_form():
    input_template = """
        <div class="input-box">
            <span class="details">{param_name}</span>
            <input type="text" name="{param}" placeholder={param_default}>
        </div>
    """
    inputs = [
        input_template.format(param="query", param_default="Prompt for image", param_name="Query")
    ]
    for p, v in NUMERIC_PARAMS.items():
        inputs.append(input_template.format(param=p, param_default=v, param_name=p[:1].upper() + p[1:].replace("_", " ")))

    return "\n".join(inputs)


def serve_pil_image(pil_img):
    img_io = BytesIO()
    pil_img.save(img_io, 'PNG')
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')


@app.route('/', methods=["GET"])
def index_page():
    form_inputs = create_form()

    if os.path.exists("static/flask_app.log"):
        logs = open("static/flask_app.log", "r").read()
    else:
        logs = "Here will be displayed logs"

    return render_template("index.html", form_inputs=form_inputs, logs=logs)


@app.route('/', methods=["POST"])
def gen():
    global _image_searcher, logger

    logger.info("\n\n\n=======NEW_REQUEST=======")

    if not _image_searcher:
        _image_searcher = ImageSearch()

    params = dict(
        image_searcher=_image_searcher,
        logger=logger,
    )

    for param in request.form:
        if request.form[param]:
            params[param] = request.form[param]

    return serve_pil_image(create_collage(**params))


if __name__ == "__main__":
    cli = FlaskGroup(app)
    cli()
